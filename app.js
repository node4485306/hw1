const http = require('http');
const commandLineArgs = require('command-line-args');

let requestCount = 0;

const optionDefinitions = [
  { name: 'port', alias: 'p', type: Number }
];

const options = commandLineArgs(optionDefinitions);

const server = http.createServer((req, res) => {
  requestCount += 1;
  res.end(JSON.stringify({
    message: 'Request handled successfully',
    requestCount,
  }));
});

const port = options.port || 3000;

server.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
